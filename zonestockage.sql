﻿-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 30 Janvier 2017 à 14:33
-- Version du serveur :  5.7.17-0ubuntu0.16.04.1
-- Version de PHP :  7.0.13-0ubuntu0.16.04.1


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";




/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


--
-- Base de données :  `zonestockage`
--
CREATE DATABASE IF NOT EXISTS `zonestockage` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `zonestockage`;


-- --------------------------------------------------------


--
-- Structure de la table `bloc`
--


CREATE TABLE `bloc` (
  `codeBloc` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Contenu de la table `bloc`
--


INSERT INTO `bloc` (`codeBloc`) VALUES
('A'),
('B'),
('C'),
('D'),
('E');


-- --------------------------------------------------------


--
-- Structure de la table `pile`
--


CREATE TABLE `pile` (
  `numPile` char(1) NOT NULL,
  `numTravee` char(1) NOT NULL,
  `codeBloc` char(1) NOT NULL,
  `capacite` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Contenu de la table `pile`
--


INSERT INTO `pile` (`numPile`, `numTravee`, `codeBloc`, `capacite`) VALUES
('1', '1', 'A', 10),
('1', '1', 'B', 10),
('1', '1', 'C', 10),
('1', '1', 'D', 10),
('1', '1', 'E', 10),
('1', '2', 'A', 10),
('1', '2', 'B', 10),
('1', '2', 'C', 10),
('1', '2', 'D', 10),
('1', '2', 'E', 10),
('1', '3', 'A', 10),
('1', '3', 'B', 10),
('1', '3', 'C', 10),
('1', '3', 'D', 10),
('1', '3', 'E', 10),
('2', '1', 'A', 10),
('2', '1', 'B', 10),
('2', '1', 'C', 10),
('2', '1', 'D', 10),
('2', '1', 'E', 10),
('2', '2', 'A', 10),
('2', '2', 'B', 10),
('2', '2', 'C', 10),
('2', '2', 'D', 10),
('2', '2', 'E', 10),
('2', '3', 'A', 10),
('2', '3', 'B', 10),
('2', '3', 'C', 10),
('2', '3', 'D', 10),
('2', '3', 'E', 10),
('3', '1', 'A', 10),
('3', '1', 'B', 10),
('3', '1', 'C', 10),
('3', '1', 'D', 10),
('3', '1', 'E', 10),
('3', '2', 'A', 10),
('3', '2', 'B', 10),
('3', '2', 'C', 10),
('3', '2', 'D', 10),
('3', '2', 'E', 10),
('3', '3', 'A', 10),
('3', '3', 'B', 10),
('3', '3', 'C', 10),
('3', '3', 'D', 10),
('3', '3', 'E', 10);


-- --------------------------------------------------------


--
-- Structure de la table `reservation`
--


CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `dateReservation` date NOT NULL,
  `datePrevueStockage` date NOT NULL,
  `nbJoursDeStockagePrevu` smallint(6) NOT NULL,
  `quantite` int(11) NOT NULL,
  `etat` enum('demande','encours','termine','') NOT NULL,
  `numClient` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Contenu de la table `reservation`
--


INSERT INTO `reservation` (`id`, `dateReservation`, `datePrevueStockage`, `nbJoursDeStockagePrevu`, `quantite`, `etat`, `numClient`) VALUES
(1, '2016-12-04', '2016-12-05', 5, 50, 'encours', 1),
(2, '2016-12-05', '2016-12-07', 10, 34, 'encours', 1),
(3, '2016-12-05', '2016-12-06', 8, 7, 'encours', 2),
(4, '2016-12-08', '2016-12-20', 4, 13, 'demande', 1);


-- --------------------------------------------------------


--
-- Structure de la table `reservationStockee`
--


CREATE TABLE `reservationStockee` (
  `numPile` char(1) NOT NULL,
  `numTravee` char(1) NOT NULL,
  `codeBloc` char(1) NOT NULL,
  `idReservation` int(11) NOT NULL,
  `emplacementDepart` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `dateDebutEffective` datetime NOT NULL,
  `dateFinEffective` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Contenu de la table `reservationStockee`
--


INSERT INTO `reservationStockee` (`numPile`, `numTravee`, `codeBloc`, `idReservation`, `emplacementDepart`, `quantite`, `dateDebutEffective`, `dateFinEffective`) VALUES
('1', '1', 'A', 1, 1, 4, '2016-12-05 00:00:00', '2016-12-10 00:00:00'),
('1', '1', 'A', 3, 5, 4, '2016-12-11 00:00:00', '2016-12-19 00:00:00'),
('1', '1', 'A', 4, 9, 2, '2016-12-20 00:00:00', '2016-12-24 00:00:00'),
('1', '1', 'B', 4, 1, 10, '2016-12-24 00:00:00', '2016-12-28 00:00:00'),
('1', '1', 'C', 4, 1, 1, '2016-12-28 00:00:00', '2017-01-01 00:00:00'),
('2', '1', 'A', 2, 1, 7, '2016-12-22 00:00:00', '2016-12-27 00:00:00'),
('3', '1', 'A', 2, 1, 10, '2016-11-20 00:00:00', '2016-12-01 00:00:00');


-- --------------------------------------------------------


--
-- Structure de la table `travee`
--


CREATE TABLE `travee` (
  `numTravee` char(1) NOT NULL,
  `codeBloc` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Contenu de la table `travee`
--


INSERT INTO `travee` (`numTravee`, `codeBloc`) VALUES
('1', 'A'),
('2', 'A'),
('3', 'A'),
('1', 'B'),
('2', 'B'),
('3', 'B'),
('1', 'C'),
('2', 'C'),
('3', 'C'),
('1', 'D'),
('2', 'D'),
('3', 'D'),
('1', 'E'),
('2', 'E'),
('3', 'E');


-- --------------------------------------------------------


--
-- Structure de la table `utilisateur`
--


CREATE TABLE `utilisateur` (
  `numUtilisateur` int(11) NOT NULL,
  `remember_token` varchar(100)  NULL,
  `raisonSociale` varchar(200) NOT NULL,
  `login` varchar(10) NOT NULL,
  `password` varchar(256) NOT NULL,
  `type` enum('client','fournisseur','administrateur','dsi') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Contenu de la table `utilisateur`

INSERT INTO `utilisateur` (`numUtilisateur`, `raisonSociale`, `login`, `password`, `type`, `docker`) VALUES
(1, 'SA THOLDI', 'tholdi', '*BA401E5F7BA3C4ED500DF2A4BFD611A83AD14AF2', 'client'),
(2, 'SA. TANSPORT MARITIME DU HAVRE', 'SA.TMH', '*259832BA90778A99DB69A810F6EABF486010C8BF', 'client');




--
-- Index pour les tables exportées
--


--
-- Index pour la table `bloc`
--
ALTER TABLE `bloc`
  ADD PRIMARY KEY (`codeBloc`);


--
-- Index pour la table `pile`
--
ALTER TABLE `pile`
  ADD PRIMARY KEY (`numPile`,`numTravee`,`codeBloc`),
  ADD KEY `numTraveeBloc` (`numTravee`,`codeBloc`);


--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `numClient` (`numClient`);


--
-- Index pour la table `reservationStockee`
--
ALTER TABLE `reservationStockee`
  ADD PRIMARY KEY (`numPile`,`numTravee`,`codeBloc`,`idReservation`),
  ADD KEY `idReservation` (`idReservation`),
  ADD KEY `numPileTraveeBloc` (`numPile`,`numTravee`,`codeBloc`);


--
-- Index pour la table `travee`
--
ALTER TABLE `travee`
  ADD PRIMARY KEY (`numTravee`,`codeBloc`),
  ADD KEY `codeBloc` (`codeBloc`);


--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`numUtilisateur`),
  ADD UNIQUE KEY `identifiant` (`login`);


--
-- AUTO_INCREMENT pour les tables exportées
--


--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `numUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--


--
-- Contraintes pour la table `pile`
--
ALTER TABLE `pile`
  ADD CONSTRAINT `pile_ibfk` FOREIGN KEY (`numTravee`,`codeBloc` ) REFERENCES `travee` (`numTravee`,`codeBloc`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`numClient`) REFERENCES `utilisateur` (`numUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Contraintes pour la table `reservationStockee`
--
ALTER TABLE `reservationStockee`
  ADD CONSTRAINT `reservationStockee_ibfk_1` FOREIGN KEY (`idReservation`) REFERENCES `reservation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `reservationStockee`
  ADD CONSTRAINT `reservationStockee_ibfk_2` FOREIGN KEY (`numPile`,`numTravee`,`codeBloc`) REFERENCES `pile` (`numPile`,`numTravee`,`codeBloc`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Contraintes pour la table `travee`
--
ALTER TABLE `travee`
  ADD CONSTRAINT `travee_ibfk_1` FOREIGN KEY (`codeBloc`) REFERENCES `bloc` (`codeBloc`) ON DELETE CASCADE ON UPDATE CASCADE;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



